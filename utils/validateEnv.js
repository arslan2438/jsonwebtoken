const envalid = require('envalid');

// import {
//   cleanEnv, port, str,
// } from 'envalid';

function validateEnv() {
  envalid.cleanEnv(process.env, {
    JWT_SECRET: envalid.str(),
    MONGO_PASSWORD: envalid.str(),
    MONGO_PATH: envalid.str(),
    MONGO_USER: envalid.str(),
    PORT: envalid.port(),
  });
};

module.exports  = validateEnv;

require('dotenv/config');
const express = require("express");
const bobyParser  = require("body-parser");
const mongoose  = require("mongoose");
const user  = require("./routes/users");
const validateEnv = require('./utils/validateEnv');
const app = express();
const {
	MONGO_USER,
	MONGO_PASSWORD,
	MONGO_PATH,
  } = process.env;

validateEnv();
//set up bodyParser
app.use(bobyParser.json());
app.use(bobyParser.urlencoded({extended:true}));

//Routes
app.use('/user',user);

//mongooge connection string
(async() => {
	try {
		mongoose.promise = global.promise;
		await mongoose.connect(`mongodb://${MONGO_USER}:${MONGO_PASSWORD}${MONGO_PATH}`,
		 { useNewUrlParser: true ,
		   useUnifiedTopology: true})
		console.log("DB connected..!!!");	
	} catch (e) {
		console.log("ERROR IN \"DB CONNECTIVITY\" : "  + e);	
	}
})()



//running server
app.listen( process.env.PORT, () => {
    console.log(`server started on PORT => ${process.env.PORT}`);
})

